from numpy import loadtxt
from pylab import plot,show

#Data Load
datas = loadtxt("shunt2.txt",float)
datab= loadtxt("bob2.txt",float)

x1=datas[:,0]
y1 = datas[:,1]

x2 = datab[:,0]
y2 = datab[:,1]
ivalue=[]


#time step
N = len(x1)
step = 2*10**(-8)
gconst = 3*10**7

ivalue.append(y2[0]*step)

#Integration
for j in range (1,N):
    s = 0.5*y2[0] + 0.5*y2[j] 
    for k in range(0,j+1):
        s += y2[k]
        

    ivalue.append(gconst*s*step)

    #running check
    if(j%100==0):
        print(j)
    


#x-scale
for l in range(0,N):
    x1[l]= x1[l]*10**(6)
    x2[l]= x2[l]*10**(6)
    y1[l]= y1[l]/0.1
    

#graph
plot(x2,ivalue)
plot(x1,y1)
show()



