#include <Arduino.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>


#define UBRRVAL F_CPU/8/9600-1 
// ********************************************************************************
// Function Prototypes
// ********************************************************************************


#define OUTPUT_DDR DDRD
#define OUTPUT_PIN 3

#define BAUD 9600  //9600 bits per second




#define SYSTEM_SPEED 16000000
#define CAPTURE_TIMER_PRESCALER 8 //isto assim é que está bem 
#define CAPTURE_TIMER_SPEED (SYSTEM_SPEED/CAPTURE_TIMER_PRESCALER) //isto é frequência


#define DUTY_BUFFER_SIZE 100

uint8_t capture_state = 0;
uint32_t current_start_value = 0;
uint32_t last_start_value = 0;
uint32_t last_dc_value = 0;
uint32_t overflow_value = 0;
double time = 2000000.0;

uint32_t duty_pointer;
uint32_t dc_buffer[DUTY_BUFFER_SIZE];
uint32_t null_buffer[DUTY_BUFFER_SIZE];

//static int uputc(char,FILE*);
volatile uint8_t end_flag = 0;


//static int uputc(char,FILE*);
static int put_char(char c, FILE* stream);

/* Understand this */
static FILE mystdout = FDEV_SETUP_STREAM(put_char, NULL ,_FDEV_SETUP_WRITE);

int put_char(char c, FILE* stream)
{
    // Wait if UDR is not free
    while((UCSR0A & (1<<UDRE0)) == 0);

    // Transmit data
    UDR0 = c;

    return 0;
}

// Apaga o que estiver no buffer
void clear_buffer()		
{
	static uint8_t i;
	for (i = 0; i < DUTY_BUFFER_SIZE; i++)
	{
		dc_buffer[i] = 0;
		null_buffer[i] = 0;
	}
		
	duty_pointer = 0;
}



//uint32_t vec[10];
/* Variable to store measured frequency */
uint32_t freq = 0;
/* Variable to store measured duty cycle */
uint32_t duty = 0;

/*void goToSleep(void){

set_sleep_mode(SLEEP_MODE_PWR_DOWN);
sleep_enable();
sleep_cpu();                   //go to sleep

}*/


ISR(TIMER1_CAPT_vect) 
{
	
	if(duty_pointer == DUTY_BUFFER_SIZE)
		TIMSK1 = 0 << ICIE1;
	
	if (capture_state == 0 ) {
		
		/* Save capture value. Compensate for overflow */
		current_start_value = ICR1 + overflow_value;
		/* Change polarity of trigger */
		TCCR1B = 1 << CS10 | 0 << ICES1;
		/* Calculate frequency */
		
		null_buffer[duty_pointer] = (current_start_value - last_dc_value);// isto dá 0 
		
		dc_buffer[duty_pointer] = (last_dc_value - last_start_value);
		
		/*if(freq==0)
			puts("H");*/

		duty_pointer = duty_pointer+1;
		
		/* Save last capture to use as start for next period */
		last_start_value = current_start_value & 0xffff;
		/* Change state for reading other polarity */
		capture_state = 1;
		
	} 
	
	
	else {
		/* Save capture value. Compensate for overflow */
		last_dc_value = ICR1  + overflow_value;
		/* Change polarity of trigger */
		TCCR1B = 1 << CS10 | 1 << ICES1;
		
		capture_state = 0;
	}
	overflow_value = 0;
	
	
}

/* Overflow interrupt to save when overflow occurs */
ISR(TIMER1_OVF_vect) 
{
	/* É o overflow do timer 1 */
	overflow_value = 0xffff; 
}

static void init_output_timer(void)
{
	/* Fast PWM mode */
	TCCR2A = 1 << COM2B1 | 1 << WGM21 | 1 << WGM20;
	/* Fast PWM mode, prescaler div 64 */
	TCCR2B = 1 << WGM22 | 1 << CS22;
	
	/* 16Mhz / 64 / 125 = 2000hz */
	/*TOP value */
	OCR2A = 124;
	/* Duty cycle- 40% of TOP  - see output here*/
	OCR2B = 30; 
	//OCR2A = 100;
	/* Some duty cycle value ~24% */
	//OCR2B = 30;
	/* Set PWM pin as output */
	OUTPUT_DDR = 1 << OUTPUT_PIN;
	//DDRB = 0 <<DDB0;
}

static void init_capture_timer(void)
{
	/* Prescaler = 8, Start with rising edge on waveform */
	TCCR1B = 1 << CS11 | 1 << ICES1;
	/* Enable interrupt flag */
	TIMSK1 = 1 << ICIE1;
}

void pin_config()
{
	DDRD |= (1 << DDD6);  //OC0A Pin as output (digital6)
	DDRD |= (1 << DDD5); //D5
	DDRB |= (1 << DDB5);  //Led configured as an output   00001000

}
void init_usart(void)
{
    // Set baud rate
    UBRR0H = (UBRRVAL) >> 8; //Load upper 8-bits of the BR val into the high byte of UBRR
	 
	UBRR0L = UBRRVAL;  // Load lower 8-bits of the BR val into the low byte of UBRR 
    UCSR0A = (1 << U2X0); // Asynchronous double speed mode -- para uso da fórmula com menor erro
    
    // Set frame format -- isto em falta
    UCSR0C = (3 << UCSZ00); //1 stop bit, 8-bit data

    //enable receiver, transmiter, Rx complete interrupt
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0); 
	
}


int main (void)
{

	
	// Define directions for port pins (B) 
	pin_config();
	sei();
	stdout = &mystdout;
	init_usart();
	
	//printf("123a\n");
	


	clear_buffer();

	init_capture_timer();
	init_output_timer();
	

	//delay(500); - será que o relógio corre mesmo dentro do while??!!
	while (1)
	{
		//printf("%" PRIu32 "\n",duty_pointer);
		

		if(duty_pointer == DUTY_BUFFER_SIZE ) 
		{
			
			for(int j=0; j<DUTY_BUFFER_SIZE;j++)
				//printf("%" PRIu32 "%d\n",dc_buffer[j],null_buffer[j]);
				printf("%" PRIu32 "---" "%" PRIu32 "\n", dc_buffer[j],null_buffer[j]);
				
			
			end_flag = 1;	
		}
		if(end_flag)
			break;	
	}

	

}


// usart Related
// ********************************************************************************
