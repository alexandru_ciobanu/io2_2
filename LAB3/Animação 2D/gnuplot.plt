reset

set term pdfcairo enhanced crop

set grid

set xlabel "t (s)"
set ylabel "Voltage (V)"

set xrange [0:0.6]

set output "dataOverlap.pdf"
plot "data1N.txt" u 1:2 lc rgb "red" w lines t "Lead I", \
	"data2N.txt" u 1:2 lc rgb "blue" w lines t "Lead II", \
	"data3N.txt" u 1:2 lc rgb "green" w lines t "Lead III"

set output "dataOverlapF.pdf"
plot "data1F.txt" u 1:2 lc rgb "red" w lines t "Lead I", \
	"data2F.txt" u 1:2 lc rgb "blue" w lines t "Lead II", \
	"data3F.txt" u 1:2 lc rgb "green" w lines t "Lead III"

set output "dataVOverlap.pdf"
plot "dataVF.txt" u 1:2 lc rgb "red" w lines t "aVF", \
	"dataVR.txt" u 1:2 lc rgb "blue" w lines t "aVR", \
	"dataVL.txt" u 1:2 lc rgb "green" w lines t "aVL"

set output "dataVOverlapF.pdf"
plot "dataVFF.txt" u 1:2 lc rgb "red" w lines t "aVF", \
	"dataVRF.txt" u 1:2 lc rgb "blue" w lines t "aVR", \
	"dataVLF.txt" u 1:2 lc rgb "green" w lines t "aVL"

set xrange [-0.3:0.3]
set yrange [-0.3:0.3]

set xlabel "x (V)"
set ylabel "y (V)"

set term pngcairo enhanced

set size square

set output "xy.png"
plot "xy.txt" using (0):(0):1:2 every 6 lc rgb "black" with vectors t ""
set output "xyF.png"
plot "xyF.txt" using (0):(0):1:2 every 6 lc rgb "black" with vectors t ""

set terminal gif animate delay 3.33
set output "xyAnim.gif"
stats "xy.txt" nooutput

do for [i=0:int(STATS_records)] {
    	plot "xy.txt" using (0):(0):1:2 every ::i::i lc rgb "black" with vectors t ""
	print i
}

set terminal gif animate delay 3.33
set output "xyAnimF.gif"
stats "xyF.txt" nooutput

do for [i=0:int(STATS_records)] {
    	plot "xyF.txt" using (0):(0):1:2 every ::i::i lc rgb "black" with vectors t ""
	print i
}
