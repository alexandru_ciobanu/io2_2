#include <Arduino.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>


#define UBRRVAL F_CPU/8/9600-1 
#define OUTPUT_DDR DDRD
#define OUTPUT_PIN 3

#define BAUD 9600  


#define SYSTEM_SPEED 16000000
#define CAPTURE_TIMER_PRESCALER 8
#define CAPTURE_TIMER_SPEED (SYSTEM_SPEED/CAPTURE_TIMER_PRESCALER) 

#define SIZE 48
#define DUTY_BUFFER_SIZE 120 
#define DATA_BUFFER_SIZE 255 

uint8_t capture_state = 0;
uint32_t current_start_value = 0;
uint32_t last_start_value = 0;
uint32_t last_dc_value = 0;
uint32_t overflow_value = 0;


uint32_t duty_pointer;
uint32_t dc_buffer[DUTY_BUFFER_SIZE];
uint32_t null_buffer[DUTY_BUFFER_SIZE];


volatile uint8_t end_flag = 0;



static int put_char(char c, FILE* stream);

/* Understand this */
static FILE mystdout = FDEV_SETUP_STREAM(put_char, NULL ,_FDEV_SETUP_WRITE);

int put_char(char c, FILE* stream)
{
    // Wait if UDR is not free
    while((UCSR0A & (1<<UDRE0)) == 0);

    // Transmit data
    UDR0 = c;

    return 0;
}

// Apaga o que estiver no buffer
void clear_buffer()		
{
	static uint8_t i;
	for (i = 0; i < DUTY_BUFFER_SIZE; i++)
	{
		dc_buffer[i] = 0;
		null_buffer[i] = 0;
	}
		
	duty_pointer = 0;
}




/* Variable to store measured frequency */
uint32_t freq = 0;
/* Variable to store measured duty cycle */
uint32_t duty = 0;




ISR(TIMER1_CAPT_vect) 
{
	
	if(duty_pointer == DUTY_BUFFER_SIZE)
		TIMSK1 = 0 << ICIE1;
	
	if (capture_state == 0 ) {
		
		/* Save capture value. Compensate for overflow */
		current_start_value = ICR1 + overflow_value;
		/* Change polarity of trigger */
		TCCR1B = 1 << CS10 | 0 << ICES1;
		/* Calculate frequency */
		
		null_buffer[duty_pointer] = (current_start_value - last_dc_value)/SIZE;
		
		dc_buffer[duty_pointer] = (last_dc_value - last_start_value)/SIZE;
		
		

		duty_pointer = duty_pointer+1;
		
		/* Save last capture to use as start for next period */
		last_start_value = current_start_value & 0xffff;
		/* Change state for reading other polarity */
		capture_state = 1;
		
	} 
	
	
	else {
		/* Save capture value. Compensate for overflow */
		last_dc_value = ICR1  + overflow_value;
		/* Change polarity of trigger */
		TCCR1B = 1 << CS10 | 1 << ICES1;
		
		capture_state = 0;
	}
	overflow_value = 0;
	
	
}

/* Overflow interrupt to save when overflow occurs */
ISR(TIMER1_OVF_vect) 
{
	
	overflow_value = 0xffff; 
}

static void init_output_timer(void)
{
	/* Fast PWM mode */
	TCCR2A = 1 << COM2B1 | 1 << WGM21 | 1 << WGM20;
	/* Fast PWM mode, prescaler div 32 - about 1kHz */
	TCCR2B = 1 << WGM22 | 1 << CS22 | 0<<CS21| 0<<CS20;
	
	
	/*TOP value */
	OCR2A = 124;
	/* Duty cycle- 40% of TOP  - see output here*/
	OCR2B = 30; 

	/* Set PWM pin as output */
	OUTPUT_DDR = 1 << OUTPUT_PIN;
	//DDRB = 0 <<DDB0;
}

static void init_capture_timer(void)
{
	/* Prescaler = 8, Start with rising edge on waveform */
	TCCR1B = 0<< CS11 | 1 << ICES1;
	/* Enable interrupt flag */
	TIMSK1 = 1 << ICIE1;
}

void pin_config()
{
	DDRD |= (1 << DDD6);  //OC0A Pin as output (digital6)
	DDRD |= (1 << DDD5); //D5
	DDRB |= (1 << DDB5);  //Led configured as an output   00001000

}
void init_usart(void)
{
    // Set baud rate
    UBRR0H = (UBRRVAL) >> 8; //Load upper 8-bits of the BR val into the high byte of UBRR
	 
	UBRR0L = UBRRVAL;  // Load lower 8-bits of the BR val into the low byte of UBRR 
    UCSR0A = (1 << U2X0); // Asynchronous double speed mode -- 
    
    // Set frame format 
    UCSR0C = (3 << UCSZ00); //1 stop bit, 8-bit data

    //enable receiver, transmiter, Rx complete interrupt
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0); 
	
}


int main (void)
{

	uint32_t data[DATA_BUFFER_SIZE];
	// Define directions for port pins (B) 
	pin_config();
	sei();
	stdout = &mystdout;
	init_usart();
	

	
	clear_buffer();

	init_capture_timer();
	init_output_timer();
	


	while (1)
	{
		
		

		if(duty_pointer == DUTY_BUFFER_SIZE ) 
		{
			/* print length */
			printf("WHITES---- BLACKS\n");
			for(int j=0; j<DUTY_BUFFER_SIZE;j++)
			{
				
				printf("%" PRIu32 "---", dc_buffer[j]);
				printf("%" PRIu32 "---\n", null_buffer[j]);
			}
				
			end_flag = 1;	
		}
		if(end_flag)
			break;	
	}
	
	
	
	printf("---------DATA CONVERSION----------\n");
	int k = 0; // index of Data vector
	for(int j=0; j<DUTY_BUFFER_SIZE;j++)
	{
		
		
		for(int l = 0;l<dc_buffer[j];l++) //conto o número de barras 
		{
			if(dc_buffer[j]<7)
			{
				// max é 6 de uma cor
					data[k] = 1;
					k = k+1;
			}
		}

		for(int l= 0;l<null_buffer[j];l++)
		{
			if(null_buffer[j]<7)
			{
				// max é 6 de uma cor
					data[k] = 0;
					k = k+1;
			}
			
		}	
	
	}

	printf("----Data Show----\n");
	for(int h = 0;h<DATA_BUFFER_SIZE;h++)
	{
		printf("%" PRIu32 "\n",data[h]);
	}
	

}

